# ES6 Client with a external Django Rest Api

This class project is built on an existing api and it's main reason to exist is to act as
client-side adaptative to any api change.

## Getting Started

At this stage our client-side offers an items list, a slider component, a contact page,
and pages for the company information data.

### Prerequisites

MongoDB ubuntu:

`sudo apt-get install -y mongodb-org`


NPM ubuntu:

`sudo apt-get install npm`

### Installing

To run this project:

`cd client/`
`npm install`

`npm run dev` ----- In case is on production, use instead `npm start`

## Running the tests

`npm test` ----- Our tests are located inside /test 's folder.

## Built With

* [NPM](https://www.npmjs.com/) - Dependency Management
* [Webpack](https://webpack.js.org/) - JS Compiler
* [Babel](https://babeljs.io/) - Transpilation tool
* [JSDOC](http://usejsdoc.org/) - Documentation


## Authors

* **Sergi Chafer** 

## Future Improvements

* Clean code, never is clean enough