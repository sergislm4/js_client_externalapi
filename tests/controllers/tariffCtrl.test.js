const $ = require('jquery');
import TariffController from '../../src/controllers/tariffCtrl';
import {SectionController} from '../../src/controllers/homeCtrl';
import {endpoint} from '../endpoints';
beforeEach(() => {
  // Set up our document body
  document.body.innerHTML = `
    <html lang="en">
        <head>
            <meta http-equiv="content-type" content="text/html; charset=utf-8">
            <title>title</title>
            <link rel="stylesheet" type="text/css" href="main.css">
            <script type="text/javascript" src="main.js"></script>
        </head>
        <body>
            <header id="nav" class="flexContainer--color-greyBackground"></header>
            
            <section class="flexContainer flexContainer--position-Center flexContainer--size-fullHeight flexContainer--color-whiteBackground">
                <aside class="sidebar sidebar--position-Left"></aside>
                <main id="main" class="flexContainer--color-whiteBackground main"></main>
                <aside class="sidebar sidebar--position-Right"></aside>
            </section>
            
            <section id="section"></section>
            <footer id="footer" class="footer flexContainer flexContainer--position-Center flexContainer--color-greyBackground"></footer>
            
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWYCfA-9ILXja6UwCg8SJw02L75Kqo_nQ"></script>
        </body>
    </html>`;
  fakeDOMLoaded();
});

test('Tariff template is printed', () => {
    new TariffController("#main");
    expect($('#titulo').text()).toBe("Tarification");
    expect($('#subtitulo').text()).toBe("Check out our offers");
});

test('Tariff template is printed with data from an endpoint /home', () => {

    new SectionController(endpoint.tarifa, "#section", 3, "Yes");
    expect(document.body.toString().search("{")).toBe(-1);
    expect($('.logo:first').attr('src')).toBe("http://127.0.0.1:8000/media/Logo/icon-egg_FRpw391.png");
    expect($('.nombretarifa:first').text()).toBe("Egg");
    expect($('.logo:last').attr('src')).toBe("http://127.0.0.1:8000/media/Logo/chick_8qOw42B.png");
    expect($('.nombretarifa:last').text()).toBe("Chick");
});

test('Test if rates pagination works', () => {
    $( 'a#pagina2' ).click();
    afterAll(() => 
    expect($('.logo:first').attr('src')).toBe("http://127.0.0.1:8000/media/Logo/icon-chicken_cA7B0et.png") &&
    expect($('.nombretarifa:first').text()).toBe("Chicken2")
    );

});

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');
  
  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}