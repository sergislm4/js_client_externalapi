const $ = require('jquery');
import CatalegController from '../../src/controllers/catalegCtrl';
import {endpoint} from '../endpoints';
beforeEach(() => {
  // Set up our document body
  document.body.innerHTML = `
    <html lang="en">
        <head>
            <meta http-equiv="content-type" content="text/html; charset=utf-8">
            <title>title</title>
            <link rel="stylesheet" type="text/css" href="main.css">
            <script type="text/javascript" src="main.js"></script>
        </head>
        <body>
            <header id="nav" class="flexContainer--color-greyBackground"></header>
            
            <section class="flexContainer flexContainer--position-Center flexContainer--size-fullHeight flexContainer--color-whiteBackground">
                <aside class="sidebar sidebar--position-Left"></aside>
                <main id="main" class="flexContainer--color-whiteBackground main"></main>
                <aside class="sidebar sidebar--position-Right"></aside>
            </section>
            
            <section id="section"></section>
            <footer id="footer" class="footer flexContainer flexContainer--position-Center flexContainer--color-greyBackground"></footer>
            
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWYCfA-9ILXja6UwCg8SJw02L75Kqo_nQ"></script>
        </body>
    </html>`;
  fakeDOMLoaded();
});

test('Products template is printed with data from an endpoint /home', () => {
    new CatalegController(endpoint.productos, endpoint.filtros, "#main", 3, "Yes");
    expect(document.body.toString().search("{")).toBe(-1);
});

test('Test if rates pagination works', () => {
    $( 'a#pagina2' ).click();
    afterAll(() => 
    expect($('.nombreproducto:first').text()).toBe("BQ A 4.5")
    );
});

test('Test if filter using brand works', () => {
    $("#marca").val("1");
    afterAll(() => 
    expect($('.main--position-quarter').length).toBe(3)
    );
});

test('Test if filter using screen works', () => {
    $("#pantalla").val("1");
    afterAll(() => 
    expect($('.main--position-quarter').length).toBe(2)
    );
});

test('Test if filter using processor works', () => {
    $("#procesador").val("1");
    afterAll(() => 
    expect($('.main--position-quarter').length).toBe(2)
    );
});

test('Test if filter using ram works', () => {
    $("#ram").val("1");
    afterAll(() => 
    expect($('.main--position-quarter').length).toBe(1)
    );
});

test('Test if filter using camara works', () => {
    $("#camara").val("2");
    afterAll(() => 
    expect($('.main--position-quarter').length).toBe(1)
    );
});

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');
  
  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}