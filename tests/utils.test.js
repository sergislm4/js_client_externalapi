import {get, setCookie, getCookie, deleteCookie} from '../src/utils';

const createMockXHR = (responseJSON) => {
    const mockXHR = {
        open: jest.fn(),
        send: jest.fn(),
        readyState: 4,
        status: 200,
        response: responseJSON || {}
    };
    return mockXHR;
}
describe('API integration test suite', function() {
    const oldXMLHttpRequest = window.XMLHttpRequest;
    let mockXHR = null;

    beforeEach(() => {
        mockXHR = createMockXHR();
        window.XMLHttpRequest = jest.fn(() => mockXHR);
    });

    afterEach(() => {
        window.XMLHttpRequest = oldXMLHttpRequest;
    });

    test('Should retrieve the list of posts from the server when calling getPosts method', function(done) {
        const reqPromise = get();
        mockXHR.response = [
            { title: 'test post' },
            { tile: 'second test post' }];
        mockXHR.onload();
        reqPromise.then((posts) => {
            expect(posts.length).toBe(2);
            expect(posts[0].title).toBe('test post');
            expect(posts[1].tile).toBe('second test post');
            done();
        });
    });

    /* test('Should return a failed promise with the error message when the API returns an error', function(done) {
        const req = get();
        mockXHR.status = 404;
        mockXHR.statusText = JSON.stringify({error: 'Failed to GET posts'});
        mockXHR.onload();
        req.then((Error) => {
            expect(Error).toBe({"error":"Failed to GET posts"});
            done();
        });
    }); */
    test('Test cookies web iteration', function() {
        setCookie('lang', 'es', 90);
        afterAll(() => 
            expect(getCookie('lang')).toBe("es")
        );
    });

    test('Cookie removal', function() {
        deleteCookie('lang');
        afterAll(() => 
            expect(getCookie('lang')).toBe(false)
        );
    });
});
