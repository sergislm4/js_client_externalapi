import {Router} from '../src/router';

beforeEach(() => {
    // Set up our document body
    document.body.innerHTML = `
      <html lang="en">
          <head>
              <meta http-equiv="content-type" content="text/html; charset=utf-8">
              <title>title</title>
              <link rel="stylesheet" type="text/css" href="main.css">
              <script type="text/javascript" src="main.js"></script>
          </head>
          <body>
              <header id="nav" class="flexContainer--color-greyBackground"></header>
              
              <section class="flexContainer flexContainer--position-Center flexContainer--size-fullHeight flexContainer--color-whiteBackground">
                  <aside class="sidebar sidebar--position-Left"></aside>
                  <main id="main" class="flexContainer--color-whiteBackground main"></main>
                  <aside class="sidebar sidebar--position-Right"></aside>
              </section>
              
              <section id="section"></section>
              <footer id="footer" class="footer flexContainer flexContainer--position-Center flexContainer--color-greyBackground"></footer>
              
              <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWYCfA-9ILXja6UwCg8SJw02L75Kqo_nQ"></script>
          </body>
      </html>`;
    fakeDOMLoaded();
});

describe('Testing routing function used to add routes, navigate between them and add listeners', function() {
    test('Adds a new route', function() {
        Router
        .add(/test/, function() {})
        expect(Router.routes[0].re.toString()).toMatch(/test/);
    });

    test('Navigate to a route', function() {
        Router.navigate('test');
        expect(window.location.href.toString()).toMatch(/test/);
        expect(Router.getFragment()).toEqual('test');
    });

    test('Check a route', function() {
        expect(Router.check("test")).toBeDefined();
    });

    test('Clear a route', function() {
        expect(Router.clearSlashes("/http://localhost:8080/#test")).toBe("http://localhost:8080/#test");
    });

    test('Remove a route', function() {
        Router
        .remove(/test/);
        expect(Router.routes).toEqual([]);
    });

    test('Remove all routes', function() {
        Router
        .add(/test1/, function() {})
        .add(/test2/, function() {})
        .flush();
        expect(Router.routes).toEqual([]);
    });

    test('Change router mode', function() {
        Router.config({ mode: 'history'});
        expect(Router.mode).toEqual('history');
    });
});

function fakeDOMLoaded() {
    const fakeEvent = document.createEvent('Event');
    
    fakeEvent.initEvent('DOMContentLoaded', true, true);
    window.document.dispatchEvent(fakeEvent);
  }