import {Router} from './router.js'; //Knows what to do for every single URL 
import {get, getCookie, setCookie} from './utils';
import {Settings} from './settings';
import NavController from './controllers/navCtrl';
import {HomeController, SectionController} from './controllers/homeCtrl';
import ContacteControler from './controllers/contacteCtrl';
import {FooterController, footer_options} from './controllers/footerCtrl';
import TariffController from './controllers/tariffCtrl';
import CatalegController from './controllers/catalegCtrl';
import {JumbotronCarousel} from './components/jumbotron_carousel' 


if(!getCookie('lang')){
    setCookie('lang', 'es', 90);
}

onInit();

Router
.add(/tariff/, function() {
    console.log("Tariff");
    new TariffController("#main");
    get('/tarifa').then(function(response) {           
        new SectionController(JSON.parse(response), "#section",3, "Yes");
    }).catch(function(error) {
            console.log("Failed!", error);
    });
}).listen()
.add(/products/, function() {
    console.log("Products");
    Promise.all([get('/articulo/'), get('/filtros/')]).then(values => { 
        new CatalegController(JSON.parse(values[0]), JSON.parse(values[1]), "#main", 3, "Yes");
    }).catch(reason => { 
        console.log(reason)
    });
}).listen()
.add(/legal/, function() {
    console.log("Aviso legal");
    get('/datos_empresa').then(function(response) {           
        new footer_options(JSON.parse(response), "#main", "legal_advise", getCookie('lang'));
    }).catch(function(error) {
        console.log("Failed!", error);
    });
})
.add(/cookies/, function() {
    console.log("Cookies");
    get('/datos_empresa').then(function(response) {           
        new footer_options(JSON.parse(response), "#main", "cookies", getCookie('lang'));
    }).catch(function(error) {
        console.log("Failed!", error);
    });
})
.add(/us/, function() {
    console.log("Cookies");
    get('/datos_empresa').then(function(response) {           
        new footer_options(JSON.parse(response), "#main", "us", getCookie('lang'));
    }).catch(function(error) {
        console.log("Failed!", error);
    });
})
.add(/contacte/, function() {
    get('/datos_empresa').then(function(response) {           
            new ContacteControler(JSON.parse(response), "#main", getCookie('lang'));
    }).catch(function(error) {
            console.log("Failed!", error);
    });
}).listen()
.add(/products\/(.*)\/edit\/(.*)/, function() {
    console.log('products', arguments);
})
.add(/home/,function() {
    console.log('Home');
    Promise.all([get('/home'), get('/tarifa/?destacado=true')]).then(values => { 
        new HomeController(JSON.parse(values[0]), "#main", getCookie('lang'));
        new SectionController(JSON.parse(values[1]), "#section", 3, "No");
    }).catch(reason => { 
        console.log(reason)
    });
    try {
        customElements.define('jumbotron-carousel', JumbotronCarousel);
    } catch (err) {
        console.log("This site uses webcomponents which don't work in all browsers! Try this site in a browser that supports them!");
    }
})


document.addEventListener("DOMContentLoaded", function(event) {
    Router.navigate('home');
}); 
  

//Fill header and footer
function onInit(){
    get('/datos_empresa').then(function(response) {           
        new NavController(JSON.parse(response), "#nav", getCookie('lang'));
        new FooterController(JSON.parse(response), "#footer", getCookie('lang'));
    }).catch(function(error) {
            console.log("Failed!", error);
    });
}


function changeLang() {
    localStorage.setItem('lang', this.value);
    location.reload(); 
}