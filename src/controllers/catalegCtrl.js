import Component from './component';

class CatalegController extends Component{
    /**
     * Carousel component constructor.
     * @constructor
     */  
    constructor(data, filters, whereTo, limit, pagination) {
        super(data, whereTo);
        /**
         * Slices results given by the previous filter into two, this is only for aesthetics in the home page
         */
        let products = data.results;
        try{
            document.querySelector(whereTo).innerHTML = this.starting_point();
        }catch(e){
            console.log("error")
        };
        /**
         * It is static because of the bad endpoint design, and has been a though decisionn considering i18n
         * It will be redesigned in a near future because of it's lack of reliability and future profness
         */
        filters.marca.forEach(function(element) {
            try{
                document.querySelector("#marca").innerHTML += `<option value="${element.id}">${element.Marca}</option>`;
            }catch(e){
                console.log("error");
            };
        });

        filters.pantalla.forEach(function(element) {
            try{
                document.querySelector("#pantalla").innerHTML += `<option value="${element.id}">${element.num_pantalla}</option>`;
            }catch(e){
                console.log("error");
            };
        });

        filters.procesador.forEach(function(element) {
            try{
                document.querySelector("#procesador").innerHTML += `<option value="${element.id}">${element.num_procesador}</option>`;
            }catch(e){
                console.log("error");
            };
        });

        filters.ram.forEach(function(element) {
            try{
                document.querySelector("#ram").innerHTML += `<option value="${element.id}">${element.num_ram}</option>`;
            }catch(e){
                console.log("error");
            };
        });

        filters.camara.forEach(function(element) {
            try{
                document.querySelector("#camara").innerHTML += `<option value="${element.id}">${element.num_camara}</option>`;
            }catch(e){
                console.log("error");
            };
        });

        paginate(products, limit, 1);
        
        if (pagination==='Yes'){
            let page_number = data.count/limit;
            document.querySelector("#paginate").innerHTML +=`<section class="main--position-full flexContainer--align-Center">
            <div class="pagination" id="pagination"></div>
            </section>`;
            for (let i = 0; i < page_number; i++) {
                if(i===0){
                    document.querySelector("#pagination").innerHTML += `<a id="pagina${i+1}" class="pagina active">${i+1}</a>`;
                }else{
                    document.querySelector("#pagination").innerHTML += `<a id="pagina${i+1}" class="pagina">${i+1}</a>`;
                }
                
            }   
        }

        const addClickEvent = elem => elem.addEventListener("click", function(event){
            document.querySelector('.active').classList.remove('active');
            document.querySelector("#pagina"+event.target.innerText).setAttribute("class", "active"); 
            paginate(products, limit, event.target.innerText);
        });

        document.querySelectorAll(".pagina").forEach(addClickEvent);
        /**
         * Future improvement
         * Change this querySelector.addEventListener to an onChange with event, to achieve a simpler solution
         * Similar to the pagination one
         */
        document.querySelector("#filter").addEventListener("click", function(){
            if(document.querySelector("#marca").selectedIndex!==0){
                products = products.filter(function (entry) { return entry.marca == document.querySelector("#marca").value; });
            }
            if(document.querySelector("#pantalla").selectedIndex!==0){
                products = products.filter(function (entry) { return entry.pantalla == document.querySelector("#pantalla").value; });
            }
            if(document.querySelector("#procesador").selectedIndex!==0){
                products = products.filter(function (entry) { return entry.procesador == document.querySelector("#procesador").value; });
            }
            if(document.querySelector("#ram").selectedIndex!==0){
                products = products.filter(function (entry) { return entry.ram == document.querySelector("#ram").value; });
            }
            if(document.querySelector("#camara").selectedIndex!==0){
                products = products.filter(function (entry) { return entry.camara == document.querySelector("#camara").value; });
            }
            if(products.length<=3){
                document.querySelector("#pagination").innerHTML="";
            }
            paginate(products, limit, 1);
        });
        
    }

    starting_point(){
        return `
        <section id="cataleg" class="main--parent">
            <section id="filters" class="main--position-full filters">
                <select id="marca" name="marca">
                    <option value="default" disabled selected>Marca</option>
                </select>
                <select id="pantalla" name="pantalla">
                    <option value="default" disabled selected>Pantalla</option>
                </select>
                <select id="procesador" name="procesador">
                    <option value="default" disabled selected>Procesador</option>
                </select>
                <select id="ram" name="ram">
                    <option value="default" disabled selected>Ram</option>
                </select>
                <select id="camara" name="camara">
                    <option value="default" disabled selected>Camara</option>
                </select>
                <input type="button" id="filter" class="button--type-filter" value="Filtrar" />
            </section>
            <section id="section" class="main--position-full">
                <section id="products" class="main--position-full main--parent"></section>
                <section id="paginate" class="main--position-full"></section>
            </section>
        </section>
        `;
    }

}


function paginate(array, page_size, page_number){
    --page_number; // because pages logically start with 1, but technically with 0
    array.slice(page_number * page_size, (page_number + 1) * page_size).forEach(function(element, index) {
        try{
            if(index===0){
                document.querySelector("#products").innerHTML = product(element);
            }else{
                document.querySelector("#products").innerHTML += product(element);
            }
        }catch(e){
            console.log("error");
        };
    });
}

let product = (element) =>`
        <article class="main--position-quarter">
            <img class="logo" src="${element.thumbnail}" width="240" height="240" />
            <h2 class="nombreproducto">${element.descripcion}</h2>
            <h2 class="precio">${element.pvp} €</h2>
            <input type="button" class="button--type-details" value="Details" />
        </article>`;

export default CatalegController;