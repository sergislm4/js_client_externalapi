import {setCookie} from '../utils';
import {languages_available} from '../lib/languages_available';
import Component from './component';

/** 
 * @class 
 * This class is used as a navigation bar controller
 */
class FooterController extends Component {
    /**
     * @constructor
     */
    constructor (data, whereTo, lang) {
        super(data, whereTo, lang);
        this.text = languages_available[lang];
        this.text = require('../lib/i18n/'+this.text+'.json');
        try {
            document.querySelector(whereTo).innerHTML =this.render(data);
        }catch(e){
            console.log("error")
        };

        document.getElementById('lang_Es').addEventListener('click', this.changeLang);
        document.getElementById('lang_Va').addEventListener('click', this.changeLang);
    }
    /** render  */
    render(data) {
        return `<section class="footer--position-mid">
            <a href="#legal">${this.text.legal_advise}</a>
            <a href="#cookies">${this.text.cookies}</a>
            <a href="#us">${this.text.about}</a>
        </section>
        <section class="footer--position-mid">
            <a id="facebook" href="${data.facebook}">Facebook</a>
            <a id="twitter" href="${data.twitter}">Twitter</a>
        </section>
        <section class="footer--position-full">
            <button id="lang_Es" value="es">es</button>
            <button id="lang_Va" value="va">va</button>
        </section>`
    }
    changeLang(){
        setCookie('lang', this.value, 90)
        location.reload();
    }
}

class footer_options {
    constructor (data, whereTo, type, lang) {
        data = data.textos.filter(function (entry) { return entry.key === type && entry.lang === lang; });
        try {
            document.querySelector(whereTo).innerHTML =this.render(data[0].content);
        }catch(e){
            console.log("error")
        };
    }
    render (data) {
        return `<div id="footer_options">${data}</div>`
    }
}


export {FooterController, footer_options};