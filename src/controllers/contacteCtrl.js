import {languages_available} from '../lib/languages_available';
import Component from './component';

/**
 * @class
 * @classdesc Class used as a controller for the route /contact
 */
class ContacteControler extends Component{
/**
 * @constructor
 */
    constructor(data, whereTo, lang) {
        super(data, whereTo, lang);
        this.text = languages_available[lang];
        this.text = require('../lib/i18n/'+this.text+'.json');
        try {
            document.querySelector(whereTo).innerHTML =this.render(data);
            this.initMap(data);
        }catch(e){
            console.log("error")
        };
    }
  
    /** render  */
    render(data) {
        return `<section class="main--parent">
            <article class="main--position-mid">
                <h1 id="name">${data.name}</h1>
                <img id="logo" src="${data.logo}" />
                <h5 id="phone">+34 ${data.phone}</h5>
                <span>${data.address},<br>${data.city},<br>${data.country}</span>
                <br><br>
            </article>
            <article class="main--position-mid">
                <form id="contact_form" class="contact_form">
                    <label for="fname">${this.text.first_name}</label>
                    <input type="text" id="fname" name="firstname" placeholder="Your name.." pattern=^[a-zA-Z][a-zA-Z0-9-_\.]{1,12}$ required>
                    <label for="email">${this.text.last_name}</label>
                    <input type="email" id="email" name="email" placeholder="Your email.." required>
                    <label for="country">${this.text.country}</label>
                    <select id="country" name="country">
                        <option value="es">España</option>
                    </select>
                    <label for="subject">${this.text.subject}</label>
                    <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px" required></textarea>
                    <input type="button" id="sendMail" class="flexContainer--align-Center button--type-send" value="${this.text.send_mail}"> 
                </form>
            </article>
            <article class="main--position-full">
                <div id="map"></div>
            </article>
        </section>`;
    }
    /**
     * @function initMap
     * Prints a map inside a div with id="map"
     */
    initMap(data) {

        /**
         * @param {Object} data
         * Data contains recieved data from /datos_empresa endpoint, including information regarding lat and long
         */
        let map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: parseFloat(data.location_lat), lng: parseFloat(data.location_long)},
            zoom: 8
        });
    }
}


/**
 * @function sendMail
 * Gets data from a form and then uses POST to send data to "/contacto" endpoint
 */

/**function sendMail(){
    
     * Gets all elements from cibtact form and returns value of each one inside a const param
     
    const form_data = Object.values(document.forms['contact_form'].elements).reduce((obj,field) => {
         obj[field.name] = field.value; return obj 
        }, {});
    get(Settings.baseURL+'/contacto/', "POST", form_data)
    .then(function(response) {
        console.log(response);
    }).catch(function(error) {
        console.log("Failed!", error);
    });

}*/

export default ContacteControler;